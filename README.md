# bloat-olmaz

A theme for the [Zola](https://www.getzola.org/) (a static site generator), which aims to be simple, accessible, responsive and fast while also not being ugly.

> You can see it in action on my blog: [bikobi.gitlab.io](https://bikobi.gitlab.io).

Features:

- [x] a very simple styling;
- [x] a responsive dark theme that follows device or browser settings (using [`prefers-color-scheme` media query](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme));
- [x] a good-looking, responsive, **one-level** navigation bar;
- [x] horizontally-scrolling code blocks and tables;
- [x] a max width for images, so that they don't overflow the page's width;
- [x] atom feed for blog posts (default in Zola);

---

[[_TOC_]]

## Usage

### Create a website and configure it to use *bloat-olmaz*

1. Create a site with Zola:
    ```
    zola init mysite
    ```
1. Move to the website's root directory:
    ```
    cd mysite/
    ```
1. Initialize a Git repository:
    ```
    git init
    ```
1. To configure your menu's (navbar's) sections, paste the following at the end of `config.toml`, in the `[extra]` section:
    ```
    menu = [
        {url = "/#", name = "Home"},
        {url = "/about", name = "About"},
        {url = "/blog", name = "Blog"},
        {url = "/contacts", name = "Contacts"},
        {url = "/blog/atom.xml", name = "Feed"},
    ]
    ```
1. To tell Zola to use `bloat-olmaz` as your theme, paste the following in the [main (unnamed) section](https://www.getzola.org/documentation/getting-started/configuration/ "Zola configuration") of `config.toml`:
    ```
    theme = "bloat-olmaz"
    ```
1. Add the theme to your website's repository, as a [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules "Git submodules"):
    ```
    # from within the root directory of your website
    git submodule add https://gitlab.com/bikobi/bloat-olmaz themes/bloat-olmaz
    ```
    This will clone the theme in your `themes/` folder, and create a `.gitmodules` file in your repository's root directory.
1. Commit the addition of the theme to your website's repository:
    ```
    git commit -am "Add bloat-olmaz theme module"
    ```

Now the theme should be applied; to test it, run `zola build && zola serve`: you shouldn't get any error and your website will be locally available.

### Add content to your website

To start adding content to your website, I recommend that you start by copying the examples in `./themes/bloat-olmaz/content/` directory and modify them, rather than starting from scratch: this will make it easier to get the hang of it.  
**Remember** that what is in your `./content/` directory must match what is in the `menu` variable in the `[extra]` section of your `config.toml`.

### Update the theme

When you need to udpate the theme, run:

```
git submodule update --remote # pull the changes
git add themes/bloat-olmaz # add them to the staging area
git commit -m "Update bloat-olmaz theme" # commit the changes
```

## Contribute

I made this theme for personal use and I'm not a professional, so any improvement is welcome, just send a merge request. These are the areas/features I'd like to work on (if you have other ideas, open an issue to discuss it):

- [ ] Making the ToC switchable on a per-post basis;
- [ ] Letting the user decide the depth of the ToC;
- [ ] Generating the navbar's sections based on `content/`, without the need for `menu` in `config.toml`;
- [ ] Improving documentation and code quality;
