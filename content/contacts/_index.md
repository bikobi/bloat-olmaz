+++
title = "Contacts"
template = "contacts.html"
+++

| platform | contact                                       |
|----------|-----------------------------------------------|
| email    | `name` at `provider.com`                      |
| phone    | +00 000 000 0000                              |
| threema  | [your-id](https://threema.id/threema-id)      |

