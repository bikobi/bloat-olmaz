+++
title = "Markdown test"
date = 1970-01-02
+++

This page teaches you the basics of markdown; the source for it is in the [theme's repo](https://gitlab.com/bikobi/bloat-olmaz/-/blob/main/content/blog/1970-01-01-markdown-tutorial/index.md). Zola makes use of [CommonMark](https://commonmark.org), with some extra features.
<!-- more -->

## Emphasis

To create bold or italic, wrap with asterisks `*` or underscores `_`.  
To avoid creating bold or italic, place a backslash in front `\*` or `\_`.

---

*Italic text*. **Bold text**. Asterisk: \*

## Paragraphs

A paragraph is consecutive lines of text with one or more blank lines between them.  
For a line break, add either a backslash `\` or two blank lines at the end of the line.


First paragraph eveniet ad laboriosam quasi. Sint in tempora odio.\
Same paragraph, new line aut reprehenderit accusantium necessitatibus ut. Autem tempore nulla architecto. Quia doloribus sapiente sed vel aperiam rerum voluptatem. Optio aut perspiciatis voluptates et aspernatur et leave a blank line.

Second paragraph eveniet quibusdam in minima officia nihil. Provident beatae enim cumque. Qui dicta eum ex harum quia ratione. Sequi nostrum minima delectus repellat facere. Voluptatum dolor qui sed doloremque enim. Qui repellat libero et perspiciatis voluptatibus.

## Headings

Starting a line with a hash `#` and a space makes a header.  
The more `#`, the smaller the header.


# Header 1
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6

## Blockquotes

To create a blockquote, start a line with greater than `>` followed by an optional space.  
Blockquotes can be nested, and can also contain other formatting.


> Everyone knows that debugging is twice as hard as writing a program in the first place.\
> So if you're as clever as you can be when you write it, how will you ever debug it? 

## Lists

Unordered lists can use either asterisks `*`, plus `+`, or hyphens `-` as list markers.  
Ordered lists use numbers followed by period `.` or right paren `)`.

Tip: when creating an ordered list, you can simply use `1.` for every list item, and Zola will take care of applying proper numbering.


- item
- item
- item

1. item 1
1. item 2
1. item 3

## Links

Links can be either inline with the text, or placed at the bottom of the text as references.  
Link text is enclosed by square brackets `[]`, and for inline links, the link URL is enclosed by parens.

A title that shows when the link is hovered can be assigned by inserting `"title"` in the parens, after the URL.


[link text](https://example.com "title")


Some paragraph [text][id].

Some other paragraphs.

[id]: https://example.com "title"

## Images

Images are almost identical to links, but an image starts with an exclamation point `!`.

The image can be either local (file's path) or public (file's url).


![alt](./image.png)

## Code

To create inline code, wrap with backticks `'`.
To create a code block, either indent each line by 4 spaces, or place 3 backticks `\`\`\`` on a line above and below the code block.

Zola supports syntax highlighting inside code blocks for most programming languages; to use it, specify the language's name after the three opening backticks.  
If you line numbers to be shown on the left, add `;linenos`.


This is `inline code`.


A code block with syntax highlighting and line numbers:

```go,linenos
package main

import "fmt"

func main() {
    fmt.Println("Hello world") // A basic hello world program written in Go.
}
```

## Nested lists

To nest one list within another, indent each item in the sublist by four spaces.  
You can also nest other elements like paragraphs, blockquotes or code blocks.


* Item
    1. First subitem
    1. Second subitem
* Item
    - Subitem
    - Subitem
* Item


## More

You can create foonotes[^1] in your posts by leveraging links syntax: just use a superscript number as the link's id[^2] and then add the address at the bottom of the page.

When inserting an image in a page, you don't have to worry about the size: just use the shortcode (insert the shortcode to use) and Zola will make sure that it does not overflow the page's width.

You can insert a horizontal rule in a page using three hyphens `---` or asterisks `***` in a new line:



[^1]: A footnote.

[^2]: Another footnote.
