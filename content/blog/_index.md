+++
title = "All your blog posts"
sort_by = "date"
template = "blog.html"
page_template = "blog-page.html"
insert_anchor_links = "right"
generate_feed = true
+++

With `generate_feed = true`, Zola generates an Atom feed people can use to subscribe to your blog, located at `<baseurl>/blog/atom.xml`.

Below is a list of all the posts, most recent on top.
